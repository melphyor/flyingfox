<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="UTF-8">
    <title>Flying Fox</title>
    <meta name="Author" content="" />
    <link href='https://fonts.googleapis.com/css?family=Raleway:200' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link rel="shortcut icon" type="image/png" href="img/favicon.png" />
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">



</head>

<body>
    <header>
        <div class="menu-icon">
            <span class="menu-icon__line menu-icon__line-left"></span>
            <span class="menu-icon__line"></span>
            <span class="menu-icon__line menu-icon__line-right"></span>
        </div>

        <div class="nav">
            <div class="nav__content">
                <ul class="nav__list">
                    <li class="nav__list-item"><a href="#">Register</a></li>
                    <li class="nav__list-item"><a href="login-fox.php">Log In</a></li>
                </ul>
            </div>
        </div>

        <div class="site-content">
            <h1 class="site-content__headline">Vilnius Travellers Community</h1>
        </div>
    </header>



    <!-- About Us, What we Do, Our Portfolio, Contacts -->


    <article class="first-links">
        <ul class="scroll">
            <li><span>Welcome</span></li>
            <li><span><a href="#about">About Us</a></span></li>
            <li><span><a href="#wedo">What we do</a></span></li>
            <li><span><a href="#portfolio">Our portfolio</a></span></li>
            <li><span><a href="#contacts">Contact us</a></span></li>
        </ul>
    </article>


    <div class="clip-top top">
        <p class="space-top first" id="about">
            We are based since 2014.<br> We visited 27 countries in Europe<br> 3 countries in Asia &amp;<br> 2 countries in South Amerika
        </p>
    </div>




    <!-- Sliding Pictures -->

    <article id="wedo">
        <div class="skw-pages">
            <div class="skw-page skw-page-1 active">
                <div class="skw-page__half skw-page__half--left">
                    <div class="skw-page__skewed">
                        <div class="skw-page__content"></div>
                    </div>
                </div>
                <div class="skw-page__half skw-page__half--right">
                    <div class="skw-page__skewed">
                        <div class="skw-page__content">
                            <h2 class="skw-page__heading">Point One</h2>
                            <p class="skw-page__description">Exloring non touristic pathes</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="skw-page skw-page-2">
                <div class="skw-page__half skw-page__half--left">
                    <div class="skw-page__skewed">
                        <div class="skw-page__content">
                            <h2 class="skw-page__heading">Point two</h2>
                            <p class="skw-page__description">Taking photos to inspire others to travel</p>
                        </div>
                    </div>
                </div>
                <div class="skw-page__half skw-page__half--right">
                    <div class="skw-page__skewed">
                        <div class="skw-page__content"></div>
                    </div>
                </div>
            </div>
            <div class="skw-page skw-page-3">
                <div class="skw-page__half skw-page__half--left">
                    <div class="skw-page__skewed">
                        <div class="skw-page__content"></div>
                    </div>
                </div>
                <div class="skw-page__half skw-page__half--right">
                    <div class="skw-page__skewed">
                        <div class="skw-page__content">
                            <h2 class="skw-page__heading">Point three</h2>
                            <p class="skw-page__description">Exploaring beauty of every season</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="skw-page skw-page-4">
                <div class="skw-page__half skw-page__half--left">
                    <div class="skw-page__skewed">
                        <div class="skw-page__content">
                            <h2 class="skw-page__heading">Point four</h2>
                            <p class="skw-page__description">Camping in the woods &amp; mountains to observe wild animals</p>
                        </div>
                    </div>
                </div>
                <div class="skw-page__half skw-page__half--right">
                    <div class="skw-page__skewed">
                        <div class="skw-page__content"></div>
                    </div>
                </div>
            </div>
            <div class="skw-page skw-page-5">
                <div class="skw-page__half skw-page__half--left">
                    <div class="skw-page__skewed">
                        <div class="skw-page__content"></div>
                    </div>
                </div>
                <div class="skw-page__half skw-page__half--right">
                    <div class="skw-page__skewed">
                        <div class="skw-page__content">
                            <h2 class="skw-page__heading">Point five</h2>
                            <p class="skw-page__description">
                                Capturing the beauty moments of our lives
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>


    <!-- Sliding Pictures for small screens -->

    <article class="wedo-small">
        <!-- Slideshow container -->
        <div class="slideshow-container">

            <!-- Full-width images with number and caption text -->
            <div class="mySlides fade">
                <div class="numbertext">1 / 5</div>
                <img src="img/iceland.jpg" style="width:100%">
                <div class="text">Exloring non touristic pathes</div>
            </div>

            <div class="mySlides fade">
                <div class="numbertext">2 / 5</div>
                <img src="img/mountain.jpg" style="width:100%">
                <div class="text">Taking photos to inspire others to travel</div>
            </div>

            <div class="mySlides fade">
                <div class="numbertext">3 / 5</div>
                <img src="img/road.jpg" style="width:100%">
                <div class="text">Exploaring beauty of every season</div>
            </div>

            <div class="mySlides fade">
                <div class="numbertext">4 / 5</div>
                <img src="img/wood.jpg" style="width:100%">
                <div class="text">Camping in the woods &amp; mountains to observe wild animals</div>
            </div>

            <div class="mySlides fade">
                <div class="numbertext">5 / 5</div>
                <img src="img/haybays.jpg" style="width:100%">
                <div class="text">
                    <p>Capturing the beauty moments of our lives</p>
                </div>
            </div>

            <!-- Next and previous buttons -->
            <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
            <a class="next" onclick="plusSlides(1)">&#10095;</a>
        </div>
        <br>

        <!-- The dots/circles -->
        <div style="text-align:center">
            <span class="dot" onclick="currentSlide(1)"></span>
            <span class="dot" onclick="currentSlide(2)"></span>
            <span class="dot" onclick="currentSlide(3)"></span>
        </div>
    </article>





    <!-- Empty Block and Link to the Blog -->

    <div class="clip-top bottom">
        <p class="space-top second"></p>
    </div>


    <div class="clip-top top second">
        <p class="space-top third" id="portfolio">Click to Enter our Blog
            <a href="blog.php"><button type="button" class="arrow-button"><i class="fas fa-arrow-right"></i></button></a>
        </p>
    </div>





    <!-- Members' Photos -->

    <div>
        <div style="height: 500px;" class="links">
            <div class="link active">
                <h2>Page 1</h2>
                <p class="content name" style="text-align: center;">Who We Are</p>
                <p class="content whoweare">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa dicta maiores perferendis doloribus quibusdam libero. Quia tempora a, vitae totam deleniti, quisquam qui voluptatum deserunt cum minima sed id atque vero. Dolorum voluptatem sapiente vel numquam deleniti quibusdam ullam praesentium, doloremque, explicabo debitis eum nisi in deserunt laudantium ea odit officiis consequuntur impedit dolor reiciendis fuga esse optio! Similique adipisci quaerat, asperiores! Quas consequuntur ad impedit vitae modi obcaecati minima enim iste laboriosam doloribus dolorem esse fugit voluptatibus, aliquid nihil nostrum et. Maiores, ullam odio sit incidunt provident nisi voluptatibus voluptatem et magnam quos, beatae numquam id recusandae natus. Commodi.</p>
            </div>
            <div class="link" data-color="#777">
                <h2>Page 2</h2>
                <img src="photos/Rimvydas.jpg" class="photos">
                <p class="content name">Rimvydas B</p>
                <p class="content">Organisator and leader. Without any doubts Rimvydas is the best person to organise our travellings. He is able to pay attention on all details and save us from any troubles. He is the person who always has the plan B in case if plan A fails. And if plan B fails, he will always in a matter of the seconds will find the plan C.</p>
            </div>
            <div class="link">
                <h2>Page 3</h2>
                <img src="photos/Angelika.jpeg" class="photos shadow">
                <p class="content name">Angelika S</p>
                <p class="content">The founder of out community who is responsible for paperwork and administrative side of our daily duties. If you call us or e-mail us she will be the first who will respond to all your queries.</p>
            </div>
            <div class="link" data-color="#777">
                <h2>Page 4</h2>
                <img src="photos/Mantas.jpeg" class="photos">
                <p class="content name">Mantas p</p>
                <p class="content">He joined our team the last and became not replaceble member of our community. Mantas is inspiring us with a great ideas and inovations. The main point of his responsibilities is to find new pathes and activities for our travels.</p>
            </div>
        </div>
    </div>


    <!-- Members' Info for smaller displays -->
    <div class="members-small-display">
        <button class="small1 smal">Our Team</button>
        <div class="info1 inf">
            <p class="content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa dicta maiores perferendis doloribus quibusdam libero. Quia tempora a, vitae totam deleniti, quisquam qui voluptatum deserunt cum minima sed id atque vero. Dolorum voluptatem sapiente vel numquam deleniti quibusdam ullam praesentium, doloremque, explicabo debitis eum nisi in deserunt laudantium ea odit officiis consequuntur impedit dolor reiciendis fuga esse optio! Similique adipisci quaerat, asperiores! Quas consequuntur ad impedit vitae modi obcaecati minima enim iste laboriosam doloribus dolorem esse fugit voluptatibus, aliquid nihil nostrum et. Maiores, ullam odio sit incidunt provident nisi voluptatibus voluptatem et magnam quos, beatae numquam id recusandae natus. Commodi.</p>
        </div>

        <button class="small2 smal">Manager</button>
        <div class="info2 inf">
            <img src="photos/Rimvydas.jpg" class="photos">
            <p class="content name">Rimvydas B</p>
            <p class="content">Organisator and leader. Without any doubts Rimvydas is the best person to organise our travellings. He is able to pay attention on all details and save us from any troubles. He is the person who always has the plan B in case if plan A fails. And if plan B fails, he will always in a matter of the seconds will find the plan C.</p>
        </div>

        <button class="small3 smal">Administrator</button>
        <div class="info3 inf">
            <img src="photos/Angelika.jpeg" class="photos shadow">
            <p class="content name">Angelika S</p>
            <p class="content">The founder of out community who is responsible for paperwork and administrative side of our daily duties. If you call us or e-mail us she will be the first who will respond to all your queries.</p>
        </div>

        <button class="small4 smal">Creator</button>
        <div class="info4 inf">
            <img src="photos/Mantas.jpeg" class="photos">
            <p class="content name">Mantas p</p>
            <p class="content">He joined our team the last and became not replaceble member of our community. Mantas is inspiring us with a great ideas and inovations. The main point of his responsibilities is to find new pathes and activities for our travels.</p>
        </div>
    </div>






    <!-- MAP & Contacts -->



    <article class="page-footer" id="contacts" style="background-color: #292929">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">How to find us</h5>
                    <p><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2304.9845435796187!2d25.29012811570362!3d54.709895079403594!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46dd96ae1dedd637%3A0x73978617430c730c!2sUlon%C5%B3+g.+5%2C+Vilnius+08240!5e0!3m2!1slt!2slt!4v1522422811935" width="500" height="300" frameborder="1" style="border:0" allowfullscreen></iframe></p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 style="margin-top:100px">Contacts</h5>
                    <ul style="margin-top:20px">
                        <li>Flying Fox Travelling Community</li>
                        <li>Ulonu street 5, LT-03113 Vilnius</li>
                        <li>Phone: +370 606 75071</li>
                        <li>E-mail: flyingfox@gmail.com</li>
                    </ul>
                </div>
            </div>
        </div>
    </article>





    <!-- Footer -->

    <footer>
        <ul class="footer-list2">
            <li><a href="#"><i class="fab fa-facebook-square"></i></a></li>
            <li><a href="#"><i class="fab fa-twitter-square"></i></a></li>
            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
            <li><a href="#"><i class="fab fa-pinterest-square"></i></a></li>
        </ul>
        <p class="last-line"><span class="h-line2"></span></p>
    </footer>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/script.js"></script>
</body>

</html>
