<!DOCTYPE html>
<html>
<head>
	<title> </title>

	<meta charset="utf-8">

	<link href='https://fonts.googleapis.com/css?family=Raleway:200' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="blog.css">

</head>
<body>
<div class="row">

<?php

		$config = parse_ini_file('config.ini'); 
		$conn = mysqli_connect($config['servername'], $config['username'], $config['password'], $config['dbname']);
		if (!$conn) {
			die("Connection failed: " . mysqli_connect_error());
		}
		mysqli_set_charset($conn,"utf8");
		$sql = "SELECT COUNT(*) FROM places";
		$result = mysqli_query($conn, $sql) or trigger_error("SQL", E_USER_ERROR);
		$r = mysqli_fetch_row($result);
		$numrows = $r[0];

		// number of rows to show per page
		$rowsperpage = 6;
		// find out total pages
		$totalpages = ceil($numrows / $rowsperpage);
		if (isset($_GET['page']) && is_numeric($_GET['page'])) {
   			$currentpage = (int) $_GET['page'];
		} else {
   			// default page num
  			$currentpage = 1;
		}
		// if current page is greater than total pages...
		if ($currentpage > $totalpages) {
   			// set current page to last page
  			$currentpage = $totalpages;
		} // end if
		// if current page is less than first page...
		if ($currentpage < 1) {
   			// set current page to first page
   			$currentpage = 1;
		}
		$offset = ($currentpage - 1) * $rowsperpage;

		$sql = "SELECT * FROM places ORDER BY id DESC LIMIT $offset, $rowsperpage";
		$result = mysqli_query($conn, $sql) or trigger_error("SQL", E_USER_ERROR);
		$i = 5;
		while ($list = mysqli_fetch_assoc($result)) {
			$i++;

				echo "<div class='col s12 l4'>";
				echo "<div class='card'>";
				echo "<div class='card__image card__image--fence' style='background-image: url(" . $list['img'] . ")'></div>";
					echo "<div class='card__content'>";
						echo "<div class='card__title'>" . $list['antraste'] . "</div>";
						echo "<p class='card__text'>" . $list['tekstas'] . "</p>";
						echo "<button data-target='modal".$i."' class='btn modal-trigger'>Read More</button>";
						

						echo "<div id='modal".$i."' class='modal'>";
							echo "<div class='modal-content'>";
							echo "	<h4 class='headeris'>" . $list['antraste'] . "</h4>";
								echo "<p class='tekstas'>" . $list['tekstas'] . "</p>";
								echo "<p class='autorius'>" . $list['autorius'] . "</p>";
							echo "</div>";
							echo "<div class='modal-footer'>";
								echo "<a class='modal-action modal-close waves-effect waves-green btn-flat'>Close</a>";
							echo "</div>";
						echo "</div>";
					echo "</div>";
				echo "</div>";
	echo "</div>";
		}
		echo "<div class='col s12 m12 l12 main_box' >";
		echo "<button class='waves-effect waves-light btn-floating grey darken-2 numbers' onclick= \"location.href='index.php'\"><i class='material-icons left'>home</i></button>";
		/******  build the pagination links ******/
		// range of num links to show
		$range = 2;
		// if not on page 1, don't show back links
		if ($currentpage > 1) {
   		// show << link to go back to page 1
			echo "<button class='waves-effect waves-light btn-floating grey darken-2 numbers' onclick= \"location.href='blog.php?page=1'\"><i class='material-icons left'>first_page</i></button>";
   		// get previous page num
   			$prevpage = $currentpage - 1;
   		// show < link to go back to 1 page
   			echo "<button class='waves-effect waves-light btn-floating grey darken-2 numbers'  onclick= \"location.href='blog.php?page=$prevpage'\"><i class='material-icons left'>chevron_left</i></button>";
		}

		// loop to show links to range of pages around current page
		for ($x = ($currentpage - $range); $x < (($currentpage + $range) + 1); $x++) {
   			// if it's a valid page number...
   			if (($x > 0) && ($x <= $totalpages)) {
      			// if we're on current page...
      			if ($x == $currentpage) {
         			// 'highlight' it but don't make a link
         			echo "<button class='disabled btn-floating numbers'>$x</button>";
     				 // if not current page...
     			} else {
         			echo "<a href='blog.php?page=$x' class='waves-effect waves-light btn-floating grey darken-2 numbers'>$x</a>";
 
      			}
   			} 
		}
		// if not on last page, show forward and last page links        
		if ($currentpage != $totalpages && $totalpages != 0) {
   			$nextpage = $currentpage + 1;
   			 // echo forward link for next page 
   			echo "<button class='waves-effect waves-light btn-floating grey darken-2 numbers'  onclick= \"location.href='blog.php?page=$nextpage'\"><i class='material-icons left'>chevron_right</i></button>";
   			// echo forward link for lastpage
   			echo "<button class='waves-effect waves-light btn-floating grey darken-2 numbers'  onclick= \"location.href='blog.php?page=$totalpages'\"><i class='material-icons left'>last_page</i></button>";
		} // end if
		echo "</div>";
		/****** end build pagination links ******/
		mysqli_close($conn);
	?>

</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
<script src="blog.js"></script>
</body>
</html>