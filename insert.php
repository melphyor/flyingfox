<!DOCTYPE html>
<html>
<head>
	<title>Admin Panel</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="styles/style.css">
</head>
<body>
		<?php
		session_start(); 
 		if (!isset($_SESSION['username'])) {
  			header('location: login-fox.php');
  		}
		$config = parse_ini_file('config.ini'); 
		$conn = mysqli_connect($config['servername'], $config['username'], $config['password'], $config['dbname']);
		if (!$conn) {
			die("Connection failed: " . mysqli_connect_error());
		}
		mysqli_set_charset($conn,"utf8");


		$antraste = "";
		$tekstas = "";
		$autorius = "";
		$errors = array();

		if(isset($_POST["insert"])){
			$antraste = $_POST["antraste"];
			$tekstas = $_POST["tekstas"];
			$autorius = $_POST["autorius"];

			if(empty($_FILES["fileToUpload"]["tmp_name"])) {
				array_push($errors, "-Select image.");
			}
			if(empty($antraste)) {
				array_push($errors, "-Empty title window.");
			}
			if(empty($tekstas)) {
				array_push($errors, "-Empty text window.");
			}
			if(empty($autorius)) {
				array_push($errors, "-Empty author window.");
			}


			if (count($errors) == 0){

					$target_dir = "images/";
					$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
					$uploadOk = 1;
					$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
					$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
					$filename = $_FILES["fileToUpload"]["name"];
					$getExtension = strtolower(substr(strrchr($filename, '.'), 1));
					$pathAndExtension = $target_dir . date("Ymdhis") . "." . $getExtension;
					// Check if image file is a actual image or fake image
					if($check !== false) {
						$uploadOk = 1;
					} else {
						//echo "File is not an image.";
						//array_push($errors, "-File is not an image.");
						$uploadOk = 0;
					}
					if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
   						array_push($errors, "-Only JPG, JPEG, PNG &");
   						array_push($errors, "GIF are allowed.");
    					$uploadOk = 0;
    				}
					if ($uploadOk > 0) {
						if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $pathAndExtension)) {
							$sql_insert = "INSERT INTO places (img, antraste, tekstas, autorius)
				VALUES ('". $pathAndExtension . "', '" . $_POST["antraste"] . "', '" . $_POST["tekstas"] . "', '" . $_POST["autorius"] . "')";
							if (mysqli_query($conn, $sql_insert)) {
								echo "<script LANGUAGE='JavaScript'> 
									window.alert('Cha Cha\\nSuccess!');
    								window.location.href='adminpanel.php';
    							</script>";
								
							} else {
							echo "Error: " . $sql_insert . "<br>" . mysqli_error($conn);
							}
						} else {
							echo " Sorry, there was an error uploading your file.";
						}
					} 
			} 
		}
		?>

		<div class="forma">
   			<form class="col s12" action="insert.php" method="post" enctype="multipart/form-data">
   				<div class="card-panel insert red lighten-2"><?php include('errors.php'); ?></div>
   				<div class="file-field input-field">
     				<div class=" waves-effect waves-light btn blue">
        				<span>File</span>
        				<input type="file" name="fileToUpload" id="fileToUpload">
      				</div>
      					<div class="file-path-wrapper">
        					<input class="file-path validate" type="text">
      					</div>
    			</div>
      			<div class="row">
        			<div class="input-field col s12">
         				<input id="antr" type="text" class="validate finput" name="antraste" value='<?php echo $antraste; ?>'>
          				<label for="antr">Title</label>
        			</div>
      			</div>
      			<div class="row">
      				<div class="input-field col s12">
          				<textarea id="textarea1" class="materialize-textarea" name="tekstas" ><?php echo $tekstas; ?></textarea>
         				<label for="textarea1">Text</label>
       				 </div>
       			</div>

      			<div class="row">
        			<div class="input-field col s12">
         				<input id="autr" type="text" class="validate finput" name="autorius" value='<?php echo $autorius; ?>'>
          				<label for="autr">The author</label>
        			</div>
      			</div>
      			<a href="adminpanel.php" class="waves-effect waves-light btn blue"><i class="material-icons left">keyboard_backspace</i>Back</a>
      			<button class="waves-effect waves-light btn blue" type="submit" value="Įdėti" name="insert">Insert</button>
   			</form>

  		</div>
  		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
</body>
</html>

