<!DOCTYPE html>
<html>
<head>
	<title>Admin Panel</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="styles/style.css">
</head>
<body>
	<div class="main_box">
		<?php
			$config = parse_ini_file('config.ini'); 
			$conn = mysqli_connect($config['servername'], $config['username'], $config['password'], $config['dbname']);
			if (!$conn) {
				die("Connection failed: " . mysqli_connect_error());
			}
			mysqli_set_charset($conn,"utf8");
			$record = $_GET["id"];
			$sql = "SELECT id, img, antraste, tekstas, autorius FROM places WHERE id = " . $record . "";
			$result = mysqli_query($conn, $sql);
			$row = mysqli_fetch_assoc($result);
			mysqli_close($conn);
		?>
		<h5><?php echo $row['antraste']; ?></h5>
		<img class='paveksleliai' src=" <?php echo $row['img']; ?>">
		<h6><?php echo $row['tekstas']; ?></h6>
		<h6>Autorius: <i><?php echo $row['autorius']; ?></i></h6><br>
		<a href="news.php"><<- Atgal į naujienas</a>
	</div>
  		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
</body>
</html>