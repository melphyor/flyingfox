<!DOCTYPE html>
<html>
<head>
	<title>Naujienos</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="styles/style.css">
</head>
<body>
    <div class="main_box">
	<?php

		$config = parse_ini_file('config.ini'); 
		$conn = mysqli_connect($config['servername'], $config['username'], $config['password'], $config['dbname']);
		if (!$conn) {
			die("Connection failed: " . mysqli_connect_error());
		}
		mysqli_set_charset($conn,"utf8");
		$sql = "SELECT COUNT(*) FROM places";
		$result = mysqli_query($conn, $sql) or trigger_error("SQL", E_USER_ERROR);
		$r = mysqli_fetch_row($result);
		$numrows = $r[0];

		// number of rows to show per page
		$rowsperpage = 10;
		// find out total pages
		$totalpages = ceil($numrows / $rowsperpage);
		if (isset($_GET['page']) && is_numeric($_GET['page'])) {
   			$currentpage = (int) $_GET['page'];
		} else {
   			// default page num
  			$currentpage = 1;
		}
		// if current page is greater than total pages...
		if ($currentpage > $totalpages) {
   			// set current page to last page
  			$currentpage = $totalpages;
		} // end if
		// if current page is less than first page...
		if ($currentpage < 1) {
   			// set current page to first page
   			$currentpage = 1;
		}
		$offset = ($currentpage - 1) * $rowsperpage;

		$sql = "SELECT * FROM places ORDER BY id DESC LIMIT $offset, $rowsperpage";
		$result = mysqli_query($conn, $sql) or trigger_error("SQL", E_USER_ERROR);
		
		while ($list = mysqli_fetch_assoc($result)) {
   			echo "<div class='row'>";
   			echo "<div class='col s2'></div>";
   			echo "<div class='col s3'><img class='paveksleliai' src='" . $list['img'] . "'></div>";
      		echo "<div class='col s6'>";
      		echo "<h6>" . $list['antraste'] . "</h6>";
      		$text = $list["tekstas"];
      		if(strlen($text) > 200){
				$textdisplaylist = substr($text,0,200). "... <a href='page.php?id=". $list['id']. "'> Daugiau</a>";
			}else{
				$textdisplaylist = $text;
			} 
      		//echo "<p>" . $list['tekstas'] . "</p>";
      		echo "<p>" . $textdisplaylist . "</p>";
      		//echo "<h6>" . $list['autorius'] . "</h6>";
      		echo "</div>";
      		echo "<div class='col s1'></div>";
    		echo "</div>";
		}
		/******  build the pagination links ******/
		// range of num links to show
		$range = 2;
		// if not on page 1, don't show back links
		if ($currentpage > 1) {
   		// show << link to go back to page 1
   		//echo " <a href='adminpanel.php?page=1'><<</a> ";
			// echo "<a href='adminpanel.php?page=1' class='waves-effect waves-light btn-floating blue numbers'<i class='material-icons left'>clear</i></a>";
			echo "<button class='waves-effect waves-light btn-floating blue numbers' onclick= \"location.href='news.php?page=1'\"><i class='material-icons left'>first_page</i></button>";
   		// get previous page num
   			$prevpage = $currentpage - 1;
   		// show < link to go back to 1 page
   		//echo " <a href='adminpanel.php?page=$prevpage'><</a> ";
   		//echo "<a href='adminpanel.php?page=$prevpage' class='waves-effect waves-light btn-floating blue numbers'>$x</a>";
   			echo "<button class='waves-effect waves-light btn-floating blue numbers'  onclick= \"location.href='news.php?page=$prevpage'\"><i class='material-icons left'>chevron_left</i></button>";
		}

		// loop to show links to range of pages around current page
		for ($x = ($currentpage - $range); $x < (($currentpage + $range) + 1); $x++) {
   			// if it's a valid page number...
   			if (($x > 0) && ($x <= $totalpages)) {
      			// if we're on current page...
      			if ($x == $currentpage) {
         			// 'highlight' it but don't make a link
         			echo "<button class='disabled btn-floating numbers'>$x</button>";
     				 // if not current page...
     			} else {
         			echo "<a href='news.php?page=$x' class='waves-effect waves-light btn-floating blue numbers'>$x</a>";
 
      			}
   			} 
		}
		// if not on last page, show forward and last page links        
		if ($currentpage != $totalpages && $totalpages != 0) {
   			$nextpage = $currentpage + 1;
   			 // echo forward link for next page 
   			echo "<button class='waves-effect waves-light btn-floating blue numbers'  onclick= \"location.href='news.php?page=$nextpage'\"><i class='material-icons left'>chevron_right</i></button>";
   			// echo forward link for lastpage
   			echo "<button class='waves-effect waves-light btn-floating blue numbers'  onclick= \"location.href='news.php?page=$totalpages'\"><i class='material-icons left'>last_page</i></button>";
		} // end if
		/****** end build pagination links ******/
		mysqli_close($conn);
	?>
	</div>
	 <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>

</body>
</html>