<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Login: Flying Fox</title>
    <meta name="Author" content="" />

    <link href='https://fonts.googleapis.com/css?family=Raleway:200' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="login-fox.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/paper.js/0.11.3/paper-full.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    
    
    <link rel="shortcut icon" type="image/png" href="img/favicon.png" />
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
</head>

<body>
    
    
    <?php
		session_start();
		$config = parse_ini_file('config.ini'); 
		$conn = mysqli_connect($config['servername'], $config['username'], $config['password'], $config['dbname']);
		if (!$conn) {
			die("Connection failed: " . mysqli_connect_error());
		}
		mysqli_set_charset($conn,"utf8");


		$user = "";
		$pswd = "";
		$errors = array();

		if(isset($_POST["login"])){
			$user = $_POST["user"];
			$pswd = $_POST["pswd"];

			if(empty($user)) {
				array_push($errors, "Enter user name.");
			}
			if(empty($pswd)) {
				array_push($errors, "Enter password.");
			}

			if (count($errors) == 0){
				$pswdhash = md5($pswd);
				$sql = "SELECT * FROM users WHERE username='$user' AND password='$pswdhash'";
  				$results = mysqli_query($conn, $sql);
					if (mysqli_num_rows($results) == 1) {
  	  					$_SESSION['username'] = $user;
  	  					header('location: adminpanel.php');
  					}else {
  						array_push($errors, "Wrong user name or password.");
  					}
			} 
		}
		?>
    
    
    
    

    <div id="back">
        <canvas id="canvas" class="canvas-back"></canvas>
        <div class="backRight">
        </div>
        <div class="backLeft">
        </div>
    </div>

    <div id="slideBox">
        <div class="topLayer">
            <div class="left">
                <div class="content">
                    <h2>Sign Up</h2>
                    <form id="form-signup" method="post" onsubmit="return false;">
                        <div class="form-element form-stack">
                            <label for="email" class="form-label">Email</label>
                            <input id="email" type="email" name="email">
                        </div>
                        <div class="form-element form-stack">
                            <label for="username-signup" class="form-label">Username</label>
                            <input id="username-signup" type="text" name="username">
                        </div>
                        <div class="form-element form-stack">
                            <label for="password-signup" class="form-label">Password</label>
                            <input id="password-signup" type="password" name="password">
                        </div>
                        <div class="form-element form-checkbox">
                            <input id="confirm-terms" type="checkbox" name="confirm" value="yes" class="checkbox">
                            <label for="confirm-terms">I agree to the <a href="#">Terms of Service</a> and <a href="#">Privacy Policy</a></label>
                        </div>
                        <div class="form-element form-submit">
                            <button id="signUp" class="signup" type="submit" name="signup" disabled>Sign up</button>
                            <button id="goLeft" class="signup off">Log In</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="right">
                <div class="content">
                    <h2>Login</h2>
<!--                    <form id="form-login" method="post"  action="login-fox.php" enctype="multipart/form-data">-->
<!--                    <form id="form-login" method="post" enctype="multipart/form-data" action="login-fox.php" onsubmit="return false;">-->
                      <form id="form-login" method="post" enctype="multipart/form-data" action="login-fox.php">
                      <input type="hidden" name="login" value="1"/>
                       <div class="card-error"><?php include('errors.php'); ?></div>
                        <div class="form-element form-stack">
                            <label for="username-login" class="form-label">Username</label>
                            <input id="username-login" type="text" name="user" class="validate finput" autocomplete="off" value='<?php echo $user; ?>'>

                        </div>
                        <div class="form-element form-stack">
                            <label for="password-login" class="form-label">Password</label>
                            <input id="password-login" type="password" name="pswd" class="validate finput" value='<?php echo $pswd; ?>'>
                        </div>
                        
                    </form>
                    
                    <div class="form-element form-submit">
                            <button id="logIn" class="login" type="submit" name="login" value="submit" onclick="document.getElementById('form-login'). submit()">Log In</button>
                            <button id="goRight" class="login off" name="signup">Sign Up</button>
                            <a href="index.php" class="home"><i class="fas fa-home"></i></a>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <script src="login-fox.js"></script>
</body>

</html>
