<!DOCTYPE html>
<html>
<head>
	<title>Admin Panel</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="styles/style.css">
</head>
<body>
	<?php 
  		session_start(); 
 		 if (!isset($_SESSION['username'])) {
  			header('location: login-fox.php');
  		}
  		if (isset($_GET['logout'])) {
  			session_destroy();
  			unset($_SESSION['username']);
  			header("location: login-fox.php");
 		 }
	?>
	<div class="main_box">
		<h4>ADMIN PANEL</h4>
		<div class="ideti">
			<a href="adminpanel.php?logout='1'" class="waves-effect waves-light btn blue"><i class="material-icons left">lock_outline</i>Log Out</a>
			<a href="index.php" class="waves-effect waves-light btn blue"><i class="material-icons left">home</i>Home</a>
			<a href="insert.php" class="waves-effect waves-light btn blue"><i class="material-icons left">add</i>New</a>
		</div>
	
	<?php
		$config = parse_ini_file('config.ini'); 
		$conn = mysqli_connect($config['servername'], $config['username'], $config['password'], $config['dbname']);
		if (!$conn) {
			die("Connection failed: " . mysqli_connect_error());
		}
		mysqli_set_charset($conn,"utf8");

		// find out how many rows are in the table 
		$sql = "SELECT COUNT(*) FROM places";
		$result = mysqli_query($conn, $sql) or trigger_error("SQL", E_USER_ERROR);
		$r = mysqli_fetch_row($result);
		$numrows = $r[0];

		// number of rows to show per page
		$rowsperpage = 5;
		// find out total pages
		$totalpages = ceil($numrows / $rowsperpage);
		if (isset($_GET['page']) && is_numeric($_GET['page'])) {
   			$currentpage = (int) $_GET['page'];
		} else {
   			// default page num
  			$currentpage = 1;
		}
		// if current page is greater than total pages...
		if ($currentpage > $totalpages) {
   			// set current page to last page
  			$currentpage = $totalpages;
		} // end if
		// if current page is less than first page...
		if ($currentpage < 1) {
   			// set current page to first page
   			$currentpage = 1;
		}
		$offset = ($currentpage - 1) * $rowsperpage;

		$sql = "SELECT * FROM places ORDER BY id DESC LIMIT $offset, $rowsperpage";
		$result = mysqli_query($conn, $sql) or trigger_error("SQL", E_USER_ERROR);
		echo "<table class='striped centered'";
		while ($list = mysqli_fetch_assoc($result)) {
   			echo "<tr>";
			echo "<td>" . $list['id'] . "</td>";
			echo "<td><img class='paveksleliai' src='" . $list['img'] . "'></td>";
			//echo "<td>" . $list['img'] . "</td>";
			echo "<td class='antraste'>" . $list['antraste'] . "</td>";
			echo "<td class='tekstas'>" . $list['tekstas'] . "</td>";
			echo "<td class='autorius'>" . $list['autorius'] . "</td>";
			?>
			
			<td  class="mygtukas"><form method="POST" action="edit.php?id=<?php echo $list['id']; ?>"><input type="hidden" name="edit"/>
			<button class="waves-effect waves-light btn-floating blue" type="submit"><i class="material-icons left">edit</i></button></td></form>
			<!-- <td  class="mygtukas"><form method="POST" action="delete.php?id="><input type="hidden" name="delete"/>
			<button class="waves-effect waves-light btn-floating blue" type="submit"><i class="material-icons left">clear</i></button></td></form> -->
			<!-- <td  class="mygtukas"><form method="get">
			<button class="waves-effect waves-light btn-floating blue" onclick="deteleRecord()" type="submit"><i class="material-icons left">clear</i></button></td></form> -->
			<td  class="mygtukas"><button class="waves-effect waves-light btn-floating blue" formmethod="get" type="submit" onclick="deteleRecord(<?php echo $list['id']; ?>)" name="delete"><i class="material-icons left">clear</i></button></td>

			<?php
			echo "</tr>";
		}
		echo "</table>";
		/******  build the pagination links ******/
		// range of num links to show
		$range = 2;
		// if not on page 1, don't show back links
		if ($currentpage > 1) {
   		// show << link to go back to page 1
			echo "<button class='waves-effect waves-light btn-floating blue numbers' onclick= \"location.href='adminpanel.php?page=1'\"><i class='material-icons left'>first_page</i></button>";
   		// get previous page num
   			$prevpage = $currentpage - 1;
   		// show < link to go back to 1 page
   			echo "<button class='waves-effect waves-light btn-floating blue numbers'  onclick= \"location.href='adminpanel.php?page=$prevpage'\"><i class='material-icons left'>chevron_left</i></button>";
		}

		// loop to show links to range of pages around current page
		for ($x = ($currentpage - $range); $x < (($currentpage + $range) + 1); $x++) {
   			// if it's a valid page number...
   			if (($x > 0) && ($x <= $totalpages)) {
      			// if we're on current page...
      			if ($x == $currentpage) {
         			// 'highlight' it but don't make a link
         			//echo " [<b>$x</b>] ";
         			echo "<button class='disabled btn-floating numbers'>$x</button>";
     				 // if not current page...
     			} else {
         			//echo " <a href='adminpanel.php?page=$x'>$x</a> ";
         			echo "<a href='adminpanel.php?page=$x' class='waves-effect waves-light btn-floating blue numbers'>$x</a>";
      			}
   			} 
		}
		// if not on last page, show forward and last page links        
		if ($currentpage != $totalpages && $totalpages != 0) {
   			$nextpage = $currentpage + 1;
   			 // echo forward link for next page 
   			//echo " <a href='adminpanel.php?page=$nextpage'>></a> ";
   			echo "<button class='waves-effect waves-light btn-floating blue numbers'  onclick= \"location.href='adminpanel.php?page=$nextpage'\"><i class='material-icons left'>chevron_right</i></button>";
   			// echo forward link for lastpage
   			//echo " <a href='adminpanel.php?page=$totalpages'>>></a> ";
   			echo "<button class='waves-effect waves-light btn-floating blue numbers'  onclick= \"location.href='adminpanel.php?page=$totalpages'\"><i class='material-icons left'>last_page</i></button>";
		} // end if
		/****** end build pagination links ******/
		mysqli_close($conn);
	?>
	</div>
	<script>
	function deteleRecord(delid) {
		if (confirm("Delete record?")) {
		//window.location.href='delete.php?id=' +delid+'';
			window.location.href='delete.php?id=' +delid+''; 
		return true;
		}
	}
	</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
</body>
</html>