"use strict";

console.clear();

var app = (function () {
    var header = void 0;
    var menu = void 0;
    var menuItems = void 0;

    var init = function init() {
        header = document.querySelector("header");
        menu = document.querySelector(".menu-icon");
        menuItems = document.querySelectorAll(".nav__list-item");

        applyListeners();
    };

    var applyListeners = function applyListeners() {
        menu.addEventListener("click", function () {
            return toggleClass(header, "nav-active");
        });
    };

    var toggleClass = function toggleClass(element, stringClass) {
        if (element.classList.contains(stringClass))
            element.classList.remove(stringClass);
        else element.classList.add(stringClass);
    };

    init();
})();


/* Menu To Scroll ----------------------------------------------- */


$(document).ready(function () {

    $('.first-links').addClass('light');

    $('ul.scroll li').mouseover(function () {
        $('.first-links').addClass('dark').removeClass('light');
    });

    $('ul.scroll li').mouseout(function () {
        $('.first-links').addClass('light').removeClass('dark');
    });

})


/* Caurousel ----------------------------------------------- */

$(document).ready(function () {

    var curPage = 1;
    var numOfPages = $(".skw-page").length;
    var animTime = 1000;
    var scrolling = false;
    var pgPrefix = ".skw-page-";

    function pagination() {
        scrolling = true;

        $(pgPrefix + curPage).removeClass("inactive").addClass("active");
        $(pgPrefix + (curPage - 1)).addClass("inactive");
        $(pgPrefix + (curPage + 1)).removeClass("active");

        setTimeout(function () {
            scrolling = false;
        }, animTime);
    };

    function navigateUp() {
        if (curPage === 1) return;
        curPage--;
        pagination();
    };

    function navigateDown() {
        if (curPage === numOfPages) return;
        curPage++;
        pagination();
    };

    $(".skw-pages").on("mousewheel DOMMouseScroll", function (e) {
        e.preventDefault();
        if (scrolling) return;
        if (e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0) {
            navigateUp();
        } else {
            navigateDown();
        }
    });

    $(".skw-pages").on("keydown", function (e) {
        e.preventDefault();
        if (scrolling) return;
        if (e.which === 38) {
            navigateUp();
        } else if (e.which === 40) {
            navigateDown();
        }
    });

});


/* Gentle Scroll ----------------------------------------------- */

// Select all links with hashes
$('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function (event) {
        // On-page links
        if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
            location.hostname == this.hostname
        ) {
            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000, function () {
                    // Callback after animation
                    // Must change focus!
                    var $target = $(target);
                    $target.focus();
                    if ($target.is(":focus")) { // Checking if the target was focused
                        return false;
                    } else {
                        $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                        $target.focus(); // Set focus again
                    };
                });
            }
        }
    });



/* Who we Are Page ----------------------------------------------- */


(function iifeLinks() {
    var parent = document.querySelector(".links");
    var links, currentlyActive, windowHeight;
    var radius, borderSize, totalArea, increment, startPoint, fontSize, linkSize;

    initLinks();
    calculateSizes();

    var circle = document.createElement('div');
    styleCircle();
    addCircle();
    addLinks();
    styleLinks();

    function initLinks() {
        links = [];
        var divs = document.querySelectorAll(".link");
        for (var i = 0; i < divs.length; i++) {
            var div = divs[i];
            div.style.paddingLeft = (parent.offsetHeight * 1.1) + "px";
            links.push({
                label: div.querySelector("h2").textContent,
                bg: '#888',
                el: div /*.querySelector(".content")*/
            });
        }
        var currentlyActive = links[0];
    }

    function calculateSizes() {
        windowHeight = parent.offsetHeight;
        radius = windowHeight * 0.6;
        borderSize = radius * 0.021;
        totalArea = 48;
        increment = totalArea / (links.length - 1);
        startPoint = 0 - (totalArea / 2);
        fontSize = radius * 0.12;
        linkSize = radius * 0.25;
    }

    function styleCircle() {
        circle.style.border = borderSize + 'px solid #fff';
        circle.style.width = radius * 2 + 'px';
        circle.style.height = radius * 2 + 'px';
        circle.style.borderRadius = radius + 'px';
        circle.style.position = 'absolute';
        circle.style.top = parent.offsetTop - radius * 0.2 + 'px';
        circle.style.left = radius * -1 + 'px';
        circle.style.clip = "rect(" + (radius * 0.2) + "px 1000px " + (radius * 1.86) + "px 0px)";
    }

    function addCircle() {
        parent.appendChild(circle);
    }

    function addLinks() {
        for (var i = 0, l = links.length; i < l; i++) {
            var link = document.createElement('a'),
                hover = document.createElement('span');
            link.href = "#";
            link.dataset.color = links[i].bg;
            link.dataset.index = i;
            link.style.display = 'inline-block';
            link.style.textDecoration = 'none';
            link.style.color = '#fff';
            link.style.position = 'absolute';
            link.style.zIndex = 100;
            link.innerHTML = links[i].label;
            hover.style.position = 'absolute';
            hover.style.display = 'inline-block';
            hover.style.zIndex = 50;
            hover.style.opacity = 0;
            parent.appendChild(link);
            parent.appendChild(hover);
            link.addEventListener('mouseover', linkOver);
            link.addEventListener('mouseout', linkOut);
            links[i].elem = link;
            links[i].hover = hover;
        }
    }

    function styleLinks() {
        for (var i = 0, l = links.length; i < l; i++) {
            var link = links[i].elem,
                hover = links[i].hover,
                deg = startPoint + (i * increment);
            link.style.paddingLeft = radius * 1.2 + 'px';
            link.style.fontSize = fontSize + 'px';
            link.style.height = linkSize + 'px';
            link.style.lineHeight = linkSize + 'px';
            setTransformOrigin(link, '0px ' + linkSize * 0.5 + 'px');
            setTransition(link, 'all 0.2s ease-out');
            setTransform(link, 'rotate(' + deg + 'deg)');
            link.style.left = borderSize + 'px';
            link.style.top = parent.offsetTop + (windowHeight / 2) - (windowHeight * 0.1) + borderSize + 'px';

            hover.style.left = borderSize + 'px';
            setTransformOrigin(hover, '0px ' + linkSize * 0.5 + 'px');
            setTransition(hover, 'all 0.2s ease-out');
            setTransform(hover, 'rotate(' + deg + 'deg)');
            hover.style.top = parent.offsetTop + (windowHeight * 0.4) + borderSize + 'px';
            hover.style.width = radius + (borderSize / 2) + 'px';
            hover.style.height = linkSize + 'px';
            hover.style.borderRight = borderSize * 2 + 'px solid #fff';

        }
    }

    window.onresize = function () {
        calculateSizes();
        styleCircle();
        styleLinks();
    }

    function linkOver(e) {
        //    if (currentlyActive) {
        //      currentlyActive.el.className = "link";
        //    }

        $(".link").removeClass("active");
        var thisLink = e.target,
            thisHover = thisLink.nextSibling;
        thisLink.style.paddingLeft = radius * 1.25 + 'px';
        thisHover.style.opacity = 1;
        //    parent.style.backgroundColor = thisLink.dataset.color;
        currentlyActive = links[thisLink.dataset.index];
        currentlyActive.el.className = "link active";
        parent.style.backgroundColor = currentlyActive.el.getAttribute("data-color");
    }

    function linkOut(e) {
        var thisLink = e.target,
            thisHover = thisLink.nextSibling;
        thisLink.style.paddingLeft = radius * 1.2 + 'px';
        thisHover.style.opacity = 0;
    }

    function setTransform(element, string) {
        element.style.webkitTransform = string;
        element.style.MozTransform = string;
        element.style.msTransform = string;
        element.style.OTransform = string;
        element.style.transform = string;
    }

    function setTransformOrigin(element, string) {
        element.style.webkitTransformOrigin = string;
        element.style.MozTransformOrigin = string;
        element.style.msTransformOrigin = string;
        element.style.OTransformOrigin = string;
        element.style.transformOrigin = string;
    }

    function setTransition(element, string) {
        element.style.webkitTransition = string;
        element.style.MozTransition = string;
        element.style.msTransition = string;
        element.style.OTransition = string;
        element.style.transition = string;
    }
})();






/* ---- Carousele for small screens ---- */
var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
    showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    if (n > slides.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = slides.length
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active";
}



/* ---- To Hide buttons and to show the text on click ---- */
$(".info1").hide();
$(".small1").click(function () {
    $(".info1").slideToggle("slow");
});

$(".info2").hide();
$(".small2").click(function () {
    $(".info2").slideToggle("slow");
});

$(".info3").hide();
$(".small3").click(function () {
    $(".info3").slideToggle("slow");
});

$(".info4").hide();
$(".small4").click(function () {
    $(".info4").slideToggle("slow");
});
